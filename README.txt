CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module will show User statistics like

* Post Count
* Login Count
* Ip Address

This module provides the below facilities:
* It provides a configuration setting at (admin/config/user_statistics)
  to configure the settings.
* All the data can be seen at (/user-stats).


For a full description of the module, visit the project page:
https://www.drupal.org/project/user_statistics


REQUIREMENTS
------------

No requirements!


INSTALLATION
------------

* Install as you would normally install a contributed drupal module.
  See: https://drupal.org/documentation/install/modules-themes/modules-8
  for further information.


CONFIGURATION
-------------

* Goto: (admin/config/user_statistics).
  One settings page will be there

* Set your configuration

* Click on the save button to save the configuration.


MAINTAINERS
-----------

Current maintainers:

* mahtab_alam - https://www.drupal.org/u/mahtab_alam
* nishantkumar155 - https://www.drupal.org/u/nishantkumar155
* mahaveer003 - https://www.drupal.org/u/mahaveer003


This project has been supported by:

 * Valuebound
   Valuebound is a Drupal based enterprise Web solutions provider
   with a focus on exclusive deliverables for media & publishing industries.
 * Visit: goo.gl/mZqmKJ for more information.
