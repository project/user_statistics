<?php

namespace Drupal\user_statistics\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for User Statistics Settings.
 */
class UserStatsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'users_stats_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'user_statistics.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('user_statistics.settings');

    $form['post_count_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Post count options'),
      '#open' => TRUE,
    ];

    $form['post_count_options']['user_statistics_count_posts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Count posts'),
      '#description' => $this->t('If checked user post counts will be calculated.'),
      '#default_value' => $config->get('user_statistics_count_posts'),
    ];

    $form['post_count_options']['user_statistics_count_comments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Count comments'),
      '#description' => $this->t('If checked user comments counts will be calculated'),
      '#default_value' => $config->get('user_statistics_count_comments'),
    ];
    $node_types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $options = [];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }

    $default = array_keys($config->get('user_statistics_included_content_types'));
    $form['post_count_options']['user_statistics_included_content_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Content types to include in post count'),
      '#description' => $this->t('Select the content types to include in the user post count (hold ctrl or shift to select multiple types). Both nodes and comments will be included in the post count. If you do not select any content types, then all types will be counted.'),
      '#options' => $options,
      '#default_value' => $default,
      '#multiple' => TRUE,
      '#size' => 10,
    ];

    $form['post_count_options']['user_statistics_user_per_cron'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of users to update per cron run'),
      '#options' => [
        '10' => '10',
        '25' => '25',
        '50' => '50',
        '100' => '100',
        '200' => '200',
      ],
      '#default_value' => $config->get('user_statistics_user_per_cron'),
    ];

    $form['post_count_options']['post_count_reset'] = [
      '#type' => 'details',
      '#title' => $this->t('Post count reset'),
      '#open' => FALSE,
    ];

    $form['post_count_options']['post_count_reset']['user_statistics_reset_post_count'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset all post counts'),
      '#submit' => ['::resetPostCount'],
    ];

    $form['login_count_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Login count options'),
      '#open' => TRUE,
    ];

    $form['login_count_options']['user_statistics_count_logins'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Count logins'),
      '#description' => $this->t('If checked user login counts will be calculated.'),
      '#default_value' => $config->get('user_statistics_count_logins'),
    ];

    $form['login_count_options']['login_count_reset'] = [
      '#type' => 'details',
      '#title' => $this->t('Login count reset'),
      '#open' => FALSE,
    ];

    $form['login_count_options']['login_count_reset']['user_statistics_reset_login_count'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset all login counts'),
      '#submit' => ['::resetLoginCount'],
    ];

    $form['ip_tracking_options'] = [
      '#type' => 'details',
      '#title' => $this->t('IP address tracking'),
      '#open' => TRUE,
    ];

    $form['ip_tracking_options']['user_statistics_track_ips'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track IP addresses'),
      '#description' => $this->t('If checked the IP addresses of users will be logged. This may be a privacy issue for some people, but is very useful for discovering spammers on community sites.'),
      '#default_value' => $config->get('user_statistics_track_ips'),
    ];

    $per = [604800, 2592000, 7776000, 15724800, 31536000, 157680000, 315360000];
    $period = $this->buildOptions($per);
    $form['ip_tracking_options']['user_statistics_flush_ips_timer'] = [
      '#type' => 'select',
      '#title' => $this->t('Discard IP logs older than'),
      '#description' => $this->t('Older IP log entries will be automatically discarded. (Requires a correctly configured <a href="system/cron">cron maintenance task</a>.)'),
      '#options' => $period,
      '#default_value' => $config->get('user_statistics_flush_ips_timer'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('user_statistics.settings')
      ->set('user_statistics_count_posts', $form_state->getValue('user_statistics_count_posts'))
      ->set('user_statistics_count_comments', $form_state->getValue('user_statistics_count_comments'))
      ->set('user_statistics_included_content_types', $form_state->getValue('user_statistics_included_content_types'))
      ->set('user_statistics_user_per_cron', $form_state->getValue('user_statistics_user_per_cron'))
      ->set('user_statistics_count_logins', $form_state->getValue('user_statistics_count_logins'))
      ->set('user_statistics_track_ips', $form_state->getValue('user_statistics_track_ips'))
      ->set('user_statistics_flush_ips_timer', $form_state->getValue('user_statistics_flush_ips_timer'))
      ->set('user_block_seconds_online', $form_state->getValue('user_block_seconds_online'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Function for resetting the login count.
   */
  public function resetLoginCount(array &$form, FormStateInterface $form_state) {
    \Drupal::database()->update('user_statistics_values')
      ->fields(['login_count' => 0])->execute();
    drupal_set_message($this->t('Successfully reset Login Count'));
  }

  /**
   * Function for resetting the post count.
   */
  public function resetPostCount(array &$form, FormStateInterface $form_state) {
    \Drupal::database()->update('user_statistics_values')
      ->fields(['post_count' => 0])->execute();
    drupal_set_message($this->t('Successfully reset Post Count'));
  }

  /**
   * Function for building the Options in time interval.
   */
  public function buildOptions(array $time_intervals, $granularity = 2, $langcode = NULL) {
    $callback = function ($value) use ($granularity, $langcode) {
      return \Drupal::service('date.formatter')->formatInterval($value, $granularity, $langcode);
    };
    return array_combine($time_intervals, array_map($callback, $time_intervals));
  }

}
